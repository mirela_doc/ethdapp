import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TransferService } from './services/transfer.service';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { ListingComponent } from './pages/listing/listing.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { DetailComponent } from './pages/detail/detail.component';
import { CreateProjectComponent } from './pages/create-project/create-project.component';
import { ChartsModule } from 'ng2-charts';
import { FormsModule } from '@angular/forms';
import { NavbarComponent } from './components/navbar/navbar.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { DashletPieComponent } from './components/dashlet-pie/dashlet-pie.component';
import { DashletNumberComponent } from './components/dashlet-number/dashlet-number.component';
import { AgGridModule } from 'ag-grid-angular';
import { ViewButtonComponent } from './components/view-button/view-button.component';
import { StatusBadgeComponent } from './components/status-badge/status-badge.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SponsorModalComponent } from './components/sponsor-modal/sponsor-modal.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ProjectBodyComponent } from './components/project-body/project-body.component';
import { PostComponent } from './components/post/post.component';
import { RateCommmentComponent } from './components/rate-commment/rate-commment.component';
import { TitleComponent } from './components/title/title.component';
import { ValueFormatterComponent } from './components/value-formatter/value-formatter.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [
    AppComponent,
 SpinnerComponent, ListingComponent, DashboardComponent, ProfileComponent,
    DetailComponent, CreateProjectComponent, NavbarComponent, BreadcrumbComponent, DashletPieComponent, DashletNumberComponent, StatusBadgeComponent, ViewButtonComponent, SponsorModalComponent, ProjectBodyComponent, PostComponent, RateCommmentComponent, TitleComponent, ValueFormatterComponent],
  imports: [
    BrowserModule,
    AppRoutingModule, ChartsModule,   BrowserAnimationsModule, AgGridModule.withComponents([ViewButtonComponent]), FontAwesomeModule, ModalModule.forRoot(),     NgxMaskModule.forRoot(),
FormsModule,
    NgbModule

  ],
  providers: [TransferService],
  bootstrap: [AppComponent],
  entryComponents: [StatusBadgeComponent, SponsorModalComponent, CreateProjectComponent]
})
export class AppModule { }
