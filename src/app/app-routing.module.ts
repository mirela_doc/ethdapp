import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateProjectComponent } from './pages/create-project/create-project.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { DetailComponent } from './pages/detail/detail.component';
import { ListingComponent } from './pages/listing/listing.component';
import { ProfileComponent } from './pages/profile/profile.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent, data: { animation: 'HomePage'} },
  { path: 'profile', component: ProfileComponent, data: {animation: 'ProjectsPage'} },
  { path: 'projects', component: ListingComponent, data: {animation: 'AboutPage'} },
  { path: 'projects/open', component: ListingComponent },
  { path: 'projects/implemented', component: ListingComponent },

  { path: 'create', component: CreateProjectComponent },
  { path: 'projects/:id', component: DetailComponent },

  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
