

import { Injectable } from '@angular/core';
//import * as TruffleContract from '@truffle/contract';
import { Subject } from 'rxjs';
import contract from '@truffle/contract';
import TruffleContract from '@truffle/contract';

declare let require: any;
const Web3 = require('web3');
const token = require('../../../truffle/build/contracts/ProjectContract.json');

declare let window: any;

@Injectable({
  providedIn: 'root'
})

export class ProjectService {
  private web3Provider;
  private contracts: {};
  private account: any = null;

  private accounts: string[];
  public accountsObservable = new Subject<string[]>();
  public success: boolean;
  constructor() {
    if (typeof window.web3 === 'undefined' || (typeof window.ethereum !== 'undefined')) {
      this.web3Provider = window.ethereum || window.web3;
      window.web3 = new Web3(this.web3Provider);
    } else {
      this.web3Provider = new Web3(new Web3.providers.HttpProvider('http://localhost:7545'));

    }
    try {
      this.web3Provider.enable();
      this.success = true;
    } catch (error) {
      this.success = false;
    }
  }

  private async getAccount(): Promise<any> {
    if (this.account == null) {
      this.account = await new Promise((resolve, reject) => {

        window.web3.eth.getAccounts((err, retAccount) => {
          if (retAccount.length > 0) {
            this.account = retAccount[0];
            resolve(this.account);
          } else {
            alert('transfer.service :: getAccount :: no accounts found.');
            reject('No accounts found.');
          }
          if (err != null) {
            alert('transfer.service :: getAccount :: error retrieving account');
            reject('Error retrieving account');
          }
        });
      }) as Promise<any>;
    }
    return Promise.resolve(this.account);
  }

  public async getUserBalance(): Promise<any> {
    const account = await this.getAccount();

    return new Promise((resolve, reject) => {
      window.web3.eth.getBalance(account, function (err, balance) {

        if (!err) {
          const retVal = {
            account: account,
            balance: balance
          };

          resolve(retVal);
        } else {
          reject({ account: 'error', balance: 0 });
        }
      });
    }) as Promise<any>;
  }

  public async getProjectsCount(): Promise<any> {

    const that = this;

    var contract = require("@truffle/contract");
    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().
        then((contractInstance) => {
          return contractInstance.getProjectsCount().then((status) => {
            if (status) {
              return resolve(status);
            }
          }).catch((error) => {
            console.log(error);
            return reject('Error ');
          });
        });
    }) as Promise<any>;
  }

  public async getCommentsCount(project_id): Promise<any> {

    const that = this;

    var contract = require("@truffle/contract");
    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().
        then((contractInstance) => {
          return contractInstance.getCommentsProjectCount(project_id).then((status) => {
            if (status) {
              return resolve(status);
            }
          }).catch((error) => {
            console.log(error);
            return reject('Error ');
          });
        });
    }) as Promise<any>;
  }

  createProjectService(project) {
    const that = this;
    console.log('testing');
    console.log(that.account);
    console.log(project);
    var contract = require("@truffle/contract");

    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().then((instance) => {
        return instance.createProject(
          project.description,
          project.name,
          project.difficulty,
          project.duration, 
          project.cost,
          {
            from: that.account,
          });
      }).then((status) => {
        if (status) {
          return resolve(status);
        }
      }).catch((error) => {
        console.log(error);
        return reject('Error creating project');
      });
    });
  }


  getProject(id)
    : Promise<any> {

    const that = this;

    var contract = require("@truffle/contract");
    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().
        then((contractInstance) => {
          return contractInstance.getProjectByIndex(id).then((status) => {
            if (status) {
              return resolve(status);
            }
          }).catch((error) => {
            console.log(error);
            return reject('Error ');
          });
        });
    }) as Promise<any>;
  }



  getComment(project_id, id)
    : Promise<any> {

    const that = this;

    var contract = require("@truffle/contract");
    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().
        then((contractInstance) => {
          return contractInstance.getCommentsByProject(project_id, id).then((status) => {
            if (status) {
              return resolve(status);
            }
          }).catch((error) => {
            console.log(error);
            return reject('Error ');
          });
        });
    }) as Promise<any>;
  }




  sponsor(project_id, sponsorship_value) {
    const that = this;
    let contract = require("@truffle/contract");

    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().then((instance) => {
        return instance.sponsorProject(
          project_id,
          {
            from: that.account,
            value: window.web3.utils.toWei(sponsorship_value, 'ether')
          }
        );
      }).then((status) => {
        if (status) {
          return resolve(status);
        }
      }).catch((error) => {
        console.log(error);
        return reject('Error sponsoring');
      });
    });
  }

  close(project_id) {
    const that = this;
    let contract = require("@truffle/contract");

    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().then((instance) => {
        return instance.closeProject(
          project_id,
          {
            from: that.account
          }
        );
      }).then((status) => {
        if (status) {
          return resolve(status);
        }
      }).catch((error) => {
        console.log(error);
        return reject('Error closing project');
      });
    });
  }


  annul(project_id) {
    const that = this;
    let contract = require("@truffle/contract");

    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().then((instance) => {
        return instance.annulProject(
          project_id,
          {
            from: that.account
          }
        );
      }).then((status) => {
        if (status) {
          return resolve(status);
        }
      }).catch((error) => {
        console.log(error);
        return reject('Error annuling project');
      });
    });
  }


  enableReview(project_id) {
    const that = this;
    let contract = require("@truffle/contract");

    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().then((instance) => {
        return instance.enableProjectReview(
          project_id,
          {
            from: that.account
          }
        );
      }).then((status) => {
        if (status) {
          return resolve(status);
        }
      }).catch((error) => {
        console.log(error);
        return reject('Error sponsoring');
      });
    });
  }

  blockProject(project_id) {
    const that = this;
    let contract = require("@truffle/contract");

    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().then((instance) => {
        return instance.blockProject(
          project_id,
          {
            from: that.account
          }
        );
      }).then((status) => {
        if (status) {
          return resolve(status);
        }
      }).catch((error) => {
        console.log(error);
        return reject('Error sponsoring');
      });
    });
  }


  rate(project_id, address, comment_id, value) {
    const that = this;
    let contract = require("@truffle/contract");

    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().then((instance) => {
        return instance.rate(
          project_id,
          address,
          value,
          comment_id,
          {
            from: that.account
            // value: window.web3.utils.toWei(sponsorship_value, 'ether')
          }
        );
      }).then((status) => {
        if (status) {
          return resolve(status);
        }
      }).catch((error) => {
        console.log(error);
        return reject('Error rating');
      });
    });
  }



  createComment(project_id, comment) {
    const that = this;

    var contract = require("@truffle/contract");

    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().then((instance) => {
        return instance.createComment(
          project_id, comment,
          {
            from: that.account,
            value: window.web3.utils.toWei('0.01', 'ether')
          });
      }).then((status) => {
        if (status) {
          return resolve(status);
        }
      }).catch((error) => {
        console.log(error);
        return reject('Error creating comment');
      });
    });
  }

  cashoutProjectService( project_id) {
    const that = this;

    var contract = require("@truffle/contract");

    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().then((instance) => {
          return instance.cashOut(
            project_id,
            {
              from: that.account
            }
          );
        }).then((status) => {
          if (status) {
            return resolve(status);
          }
        }).catch((error) => {
          console.log(error);
          return reject('Error cashing out from project');
        });
    });
  }



  addAddress(address, originAccount) {
    
    const that = this;
    let contract = require("@truffle/contract");

    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().then((instance) => {
        return instance.addAddress(
        address,
          {
            from: originAccount
            // value: window.web3.utils.toWei(sponsorship_value, 'ether')
          }
        );
      }).then((status) => {
        if (status) {
          return resolve(status);
        }
      }).catch((error) => {
        console.log(error);
        return reject('Error adding address');
      });
    });
  }

  removeAddress(address, originAccount) {
    const that = this;
    let contract = require("@truffle/contract");

    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().then((instance) => {
        return instance.removeAddress(
        address,
          {
            from: originAccount
            // value: window.web3.utils.toWei(sponsorship_value, 'ether')
          }
        );
      }).then((status) => {
        if (status) {
          return resolve(status);
        }
      }).catch((error) => {
        console.log(error);
        return reject('Error removing account');
      });
    });
  }


  public async geTotalPointsCount(project_id): Promise<any> {

    const that = this;

    var contract = require("@truffle/contract");
    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().
        then((contractInstance) => {
          return contractInstance.getTotalProjectPoints(project_id).then((status) => {
            if (status) {
              return resolve(status);
            }
          }).catch((error) => {
            console.log(error);
            return reject('Error ');
          });
        });
    }) as Promise<any>;
  }



  getMyPoints(project_id, comment_id)
    : Promise<any> {

    const that = this;

    var contract = require("@truffle/contract");
    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().
        then((contractInstance) => {
          return contractInstance.myPoints(project_id, that.account, comment_id).then((status) => {
            if (status) {
              return resolve(status);
            }
          }).catch((error) => {
            console.log(error);
            return reject('Error ');
          });
        });
    });
  }

  public  getNoLow(): Promise<any> {

    const that = this;

    var contract = require("@truffle/contract");
    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().
        then((contractInstance) => {
          return contractInstance.getTotalNoLow().then((status) => {
            if (status) {
              return resolve(status);
            }
          }).catch((error) => {
            console.log(error);
            return reject('Error ');
          });
        });
    }) as Promise<any>;
  }

  public  getNoHigh(): Promise<any> {

    const that = this;

    var contract = require("@truffle/contract");
    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().
        then((contractInstance) => {
          return contractInstance. getTotalNoHigh().then((status) => {
            if (status) {
              return resolve(status);
            }
          }).catch((error) => {
            console.log(error);
            return reject('Error ');
          });
        });
    }) as Promise<any>;
  }

  public  getNoMedium(): Promise<any> {

    const that = this;

    var contract = require("@truffle/contract");
    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().
        then((contractInstance) => {
          return contractInstance.getTotalNoMedium().then((status) => {
            if (status) {
              return resolve(status);
            }
          }).catch((error) => {
            console.log(error);
            return reject('Error ');
          });
        });
    }) as Promise<any>;
  }

  public  getOpen(): Promise<any> {

    const that = this;

    var contract = require("@truffle/contract");
    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().
        then((contractInstance) => {
          return contractInstance.getOpen().then((status) => {
            if (status) {
              return resolve(status);
            }
          }).catch((error) => {
            console.log(error);
            return reject('Error ');
          });
        });
    })as Promise<any> ;
  }

  public  getAnnulled(): Promise<any> {

    const that = this;

    var contract = require("@truffle/contract");
    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().
        then((contractInstance) => {
          return contractInstance.getAnnulled().then((status) => {
            if (status) {
              return resolve(status);
            }
          }).catch((error) => {
            console.log(error);
            return reject('Error ');
          });
        });
    })as Promise<any> ;
  }


  public  getBlocked(): Promise<any> {

    const that = this;

    var contract = require("@truffle/contract");
    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().
        then((contractInstance) => {
          return contractInstance.getBlocked().then((status) => {
            if (status) {
              return resolve(status);
            }
          }).catch((error) => {
            console.log(error);
            return reject('Error ');
          });
        });
    }) as Promise<any>;
  }

  public  getImplemented(): Promise<any> {

    const that = this;

    var contract = require("@truffle/contract");
    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().
        then((contractInstance) => {
          return contractInstance.getImplemented().then((status) => {
            if (status) {
              return resolve(status);
            }
          }).catch((error) => {
            console.log(error);
            return reject('Error ');
          });
        });
    }) as Promise<any>;
  }

  public getAwaiting(): Promise<any> {

    const that = this;

    var contract = require("@truffle/contract");
    return new Promise((resolve, reject) => {
      let projectContract = contract(token);
      projectContract.setProvider(that.web3Provider);
      projectContract.deployed().
        then((contractInstance) => {
          return contractInstance.getAwaiting().then((status) => {
            if (status) {
              return resolve(status);
            }
          }).catch((error) => {
            console.log(error);
            return reject('Error ');
          });
        });
    }) as Promise<any>;
  }


public  getOwner(): Promise<any> {

  const that = this;

  var contract = require("@truffle/contract");
  return new Promise((resolve, reject) => {
    let projectContract = contract(token);
    projectContract.setProvider(that.web3Provider);
    projectContract.deployed().
      then((contractInstance) => {
        return contractInstance.getOwner().then((status) => {
          if (status) {
            return resolve(status);
          }
        }).catch((error) => {
          console.log(error);
          return reject('Error ');
        });
      });
  }) as Promise<any>;
}
}