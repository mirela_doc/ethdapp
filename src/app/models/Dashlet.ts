
export interface Dashlet { 
    value: any;
    id: string;
    name: string;
    type?: number;
    data?: any;
    labels?: any;
    key?: any;
    drill_down_url?: string;
    x?: number;
    y?: number;
    width?: number;
    height?: number;
}