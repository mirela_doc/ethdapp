

export interface Project {
    id?: string;
    name?: string;
    createdBy?: string;
    description?: string;
    cost?: number;
    duration?: number;
}
