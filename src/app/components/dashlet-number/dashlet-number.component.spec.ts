import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashletNumberComponent } from './dashlet-number.component';

describe('DashletNumberComponent', () => {
  let component: DashletNumberComponent;
  let fixture: ComponentFixture<DashletNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashletNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashletNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
