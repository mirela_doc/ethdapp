import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Dashlet } from 'src/app/models/Dashlet';

@Component({
  selector: 'app-dashlet-number',
  templateUrl: './dashlet-number.component.html',
  styleUrls: ['./dashlet-number.component.css']
})
export class DashletNumberComponent implements OnInit {
  @Input() dashletEntity: Dashlet;

  constructor(public router: Router) {
    console.log(this.dashletEntity);
  }

  ngOnInit() {
    console.log(this.dashletEntity)
  }

  public goTo(drillDownUrl: string): void {
    this.router.navigate([drillDownUrl]);
  }}