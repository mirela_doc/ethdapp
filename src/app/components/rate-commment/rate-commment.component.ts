import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-rate-commment',
  templateUrl: './rate-commment.component.html',
  styleUrls: ['./rate-commment.component.css']
})
export class RateCommmentComponent implements OnInit {
  id: any;
  account: string;
  newValue: any;
  balance: string;
  saveDisabled: boolean;

  constructor(public projectService: ProjectService, public bsModalRef: BsModalRef, private router: Router) { }

  ngOnInit(): void {
    this.getAccountAndBalance();

  }



  getAccountAndBalance = () => {
    // const that = this;
    this.projectService.getUserBalance().
      then(function (retAccount: any) {
        console.log(retAccount);
        this.account = retAccount.account;
        this.balance = retAccount.balance;
        console.log('transfer.components :: getAccountAndBalance :: that.user');
        console.log(this.account);
        // that.isLoading = false;
      }).catch(function (error) {
        console.log(error);
      });
    // this.isLoading = false;
  }

  rate() {
    this.saveDisabled = true;
    console.log(this.id, this.newValue);

    this.projectService.sponsor(this.id, this.newValue).then((r) => {
      this.saveDisabled = false;
      console.log(r);
      this.bsModalRef.hide();
      // this.router.navigate(['']);
    }).catch((e) => {
      this.saveDisabled = false;
    });
  }
}
