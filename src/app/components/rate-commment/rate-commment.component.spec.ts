import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateCommmentComponent } from './rate-commment.component';

describe('RateCommmentComponent', () => {
  let component: RateCommmentComponent;
  let fixture: ComponentFixture<RateCommmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RateCommmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateCommmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
