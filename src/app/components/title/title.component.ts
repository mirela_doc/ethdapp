import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {
  @Input() project: any;
  faAngleLeft = faAngleLeft;

  constructor(private _location: Location) {
   }

  ngOnInit(): void {
  }

  goBack(){
    this._location.back();
  }


  

}
