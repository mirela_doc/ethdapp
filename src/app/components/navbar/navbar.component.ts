import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  userDetails: any;
  owner: any;
  originAccount: any;
  constructor(public router: Router, public projectService: ProjectService) {
  }

  ngOnInit() {
    this.isOwner();

    // const that = this;
    this.projectService.getUserBalance().
      then((retAccount: any) => {
       // console.log(retAccount);
        this.originAccount = retAccount.account;
        console.log(this.originAccount);
        // that.isLoading = false;
      }).catch(function (error) {
        console.log(error);
      })
    // this.isLoading = false;

  }


  isOwner() {
    this.projectService.getOwner().then((r) => {
      this.owner = r;
      console.log(this.owner);
    }).catch((e) => {
    });
  }



}
