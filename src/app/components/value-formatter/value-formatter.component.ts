import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-value-formatter',
  templateUrl: './value-formatter.component.html',
  styleUrls: ['./value-formatter.component.css']
})
export class ValueFormatterComponent {
  value: any;

  agInit(params: any): void {
    if (params.value) {
      const milliseconds = params.value * 1000;

      const dateObject = new Date(milliseconds)
      this.value = dateObject.toLocaleString()
    }
    else {
      this.value = 0;
    }
  }
}
