import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faSave} from '@fortawesome/free-solid-svg-icons';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ProjectService } from 'src/app/services/project.service';
import { RateCommmentComponent } from '../rate-commment/rate-commment.component';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  @Input() project: any;
  @Input() comment: any;
  @Input() index: any;
  @Input() rated: any;
  account: string;
  bsModalRef: BsModalRef;
  starRating = 0;
  total = 0;
  @Output() open: EventEmitter<any> = new EventEmitter();
  faEye = faSave;
  constructor(public projectService: ProjectService, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.myPoints();
    const that = this;
    that.projectService.getUserBalance().
      then(function (retAccount: any) {
        that.account = retAccount.account;
        // that.isLoading = false;
      }).catch(function (error) {
        console.log(error);
      });
    // this.isLoading = false;
  }


  rate() {
    console.log(this.comment.addr);
    this.projectService.rate(this.project.projectId, this.comment.addr,  this.index, this.starRating).then((r) => {
      console.log(r);
      this.projectService.getComment(this.project.projectId, this.index).
        then((r: any) => {
     
          this.comment = r
         
          // this.getClosedProjects();
        }).catch((error) => {
          console.log(error);
        });
        this.projectService.getProject(this.project.projectId)
        .then((r) => {
          this.project = r;
         this.open.emit(this.project);
        }).catch((e) => {
        });

    }).catch((e) => {
    });
  }


  myPoints() {

    this.projectService.getMyPoints(this.project.projectId, this.index).then((r) => {
      console.log(r);
      this.starRating = r['words'][0];

    }).catch((e) => {
    });
  }







  convertDate(date) {
    const milliseconds = date * 1000;
    const dateObject = new Date(milliseconds);
    const value = dateObject.toLocaleString();
    return value;
  }


}
