import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { faPencilAlt, faEye } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-view-button',
  templateUrl: './view-button.component.html',
  styleUrls: ['./view-button.component.css']
})
export class ViewButtonComponent implements ICellRendererAngularComp {
  public params: any;
  url: any;
  public mapping: {};
  status: any;
  valueN: any;
  faPencil = faPencilAlt;
  faEye = faEye;


  constructor(private route: ActivatedRoute, private router: Router) {
    this.mapping = {
      1: 'Implementare',
      0: 'Propunere',
      3: 'Finalizat', 
      2: 'Blocat', 
      4: 'Anulat'
    };
  }


  agInit(params: any): void {
    this.params = params;
    this.status = params.data.state;
    this.valueN = this.mapping[this.status];
  }

  refresh(): boolean {
    return false;
  }

  view() {
    if (this.params.data.projectId) {
      this.router.navigate(['/projects', this.params.data.projectId]);
    }

  }

}
