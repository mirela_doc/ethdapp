import { Component, EventEmitter, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-sponsor-modal',
  templateUrl: './sponsor-modal.component.html',
  styleUrls: ['./sponsor-modal.component.css']
})
export class SponsorModalComponent implements OnInit {
  id: any;
  account: string;
  newValue: any;
  balance: string;
  public event: EventEmitter<any> = new EventEmitter();


  constructor(public projectService: ProjectService, public bsModalRef: BsModalRef, private router: Router) { }

  ngOnInit(): void {
    this.getAccountAndBalance();
  }



  getAccountAndBalance = () => {
    // const that = this;
    this.projectService.getUserBalance().
      then(function (retAccount: any) {
        console.log(retAccount);
        this.account = retAccount.account;
        this.balance = retAccount.balance;
        console.log('transfer.components :: getAccountAndBalance :: that.user');
        console.log(this.account);
        // that.isLoading = false;
      }).catch(function (error) {
        console.log(error);
      });
    // this.isLoading = false;
  }
    triggerEvent(item: any) {
        this.event.emit({ data: 'New' });
    }

  sponsor() {

    console.log(this.id, this.newValue);

    this.projectService.sponsor(this.id, this.newValue).then((r) => {
      console.log(r);
      

     this.bsModalRef.content.newValue = this.newValue;
     this.triggerEvent(this.newValue);

      this.bsModalRef.hide();
  
      // this.router.navigate(['']);
    }).catch((e) => {
    });
  }
}
