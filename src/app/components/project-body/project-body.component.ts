import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-project-body',
  templateUrl: './project-body.component.html',
  styleUrls: ['./project-body.component.css']
})
export class ProjectBodyComponent implements OnInit {
  @Input() project: any;  @Input() points: any;

  details = true;
  difficulty: any;
  constructor() { }

  ngOnInit(): void {
    console.log(this.project);
    
  }


  convertDate(date) {
    const milliseconds = date * 1000;
    const dateObject = new Date(milliseconds);
    const value = dateObject.toLocaleString();
    return value;
  }



}
