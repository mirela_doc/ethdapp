import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-status-badge',
  templateUrl: './status-badge.component.html',
  styleUrls: ['./status-badge.component.css']
})
export class StatusBadgeComponent {
  public mapping: {};
  @Input() public params: any;
  @Input() public status: string;
  @Input() public details: boolean;

  constructor() {
    this.mapping = {
      1: 'Implementare',
      0: 'Propunere',
      2: 'Blocat', 
      3: 'Finalizat', 
      4: 'Anulat'
    };

  }


  agInit(params: any): void {
    if (params.data.state) {
     // console.log(params.data.state['words'][0]);
      this.status = params.data.state;
    //  console.log(this.status);
    }
 
  }

}
