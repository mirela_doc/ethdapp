import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Dashlet } from 'src/app/models/Dashlet';

@Component({
  selector: 'app-dashlet-pie',
  templateUrl: './dashlet-pie.component.html',
  styleUrls: ['./dashlet-pie.component.css']
})
export class DashletPieComponent implements OnInit {

  @Input() dashletEntity: Dashlet;

  public doughnutChartType: string = 'pie';

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  // events
  public chartClicked(e: any): void {
   // this.goTo(this.dashletEntity.drill_down_url + (this.dashletEntity.values[e.active[0]._index] ? this.dashletEntity.values[e.active[0]._index] : e.active[0]._chart.config.data.labels[e.active[0]._index]));
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  public goTo(drillDownUrl: string): void {
    this.router.navigate([drillDownUrl]);
  }

  public donutColors = [
    {
      backgroundColor: [ '#495cff' , '#33b87c', '#e96e50', '#9e9e9e', '#FDB45C'
   // 'rgba(247,70,74,1)',    '#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360' 
      ]
    }
  ];

}
