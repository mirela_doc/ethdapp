import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashletPieComponent } from './dashlet-pie.component';

describe('DashletPieComponent', () => {
  let component: DashletPieComponent;
  let fixture: ComponentFixture<DashletPieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashletPieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashletPieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
