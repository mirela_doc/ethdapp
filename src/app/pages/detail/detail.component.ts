import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { SponsorModalComponent } from 'src/app/components/sponsor-modal/sponsor-modal.component';
import { Project } from 'src/app/models/Project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  Breadcrumb = [{ name: 'Acasă', to: '/dashboard', active: 'false' }, { name: 'Proiecte', to: '/projects', active: 'false' }, { name: 'Detalii proiect', to: 'projects/view/:id', active: 'true' }];

  bsModalRef: BsModalRef;
  account: string;
  address: string;
  amount: string;
  balance: string;
  success: boolean;
  createDone: boolean;
  project = {} as any;
  newComment: any;
  commentCount: any;
  points: any;
  comments = [];
  firstComment: {};
  loading: boolean;
  showAlert: boolean;
  error: boolean;
  errorMessage: any;
  showMessage: boolean;

  constructor(private projectService: ProjectService, private route: ActivatedRoute, private modalService: BsModalService) {
  }

  ngOnInit(): void {
    this.getAccountAndBalance();
    this.getProject();
    this.getComments();
    this.getTotalPoints();
  }


  getAccountAndBalance = () => {
    // const that = this;
    this.projectService.getUserBalance().
      then(function (retAccount: any) {
        console.log(retAccount);
        this.account = retAccount.account;
        this.balance = retAccount.balance;

        //that.isLoading = false;
      }).catch(function (error) {
        console.log(error);
      });
    // this.isLoading = false;
  }






  sponsorProject() {
    this.bsModalRef = this.modalService.show(SponsorModalComponent, {
      initialState: {
        id: this.route.snapshot.params['id'],
      }
    });
    this.bsModalRef.content.event.subscribe(res => {
     this.getProject();
    });
  }

  getProject() {
    this.projectService.getProject(this.route.snapshot.params['id'])
      .then((r) => {
        this.project = r;
        console.log(this.project);
        this.firstComment = {
          addr: this.project.hostAddr,
          comm: this.project.description,
          created_at: this.project.creationTime,
          points: 0
        }
      }).catch((e) => {
      });
  }

  showError() {
    this.showMessage = true;
  }

  close() {
    this.loading = true;
    this.showAlert = false;
    this.error = false;
    this.showMessage = false;
    this.projectService.close(this.route.snapshot.params['id']).then((r) => {
      this.getProject();
      this.loading = false;
      this.showAlert = true;
    }).catch((e) => {
      this.loading = false;
      this.error = true;
   this.errorMessage = 'Nu aveți permisiuni pentru marcarea proiectului ca implementat.';
    });
  }

  annul() {
    this.loading = true;
    this.showAlert = false;
    this.error = false;
    this.showMessage = false;
    this.projectService.annul(this.route.snapshot.params['id']).then((r) => {
      this.getProject();
      this.loading = false;
      this.showAlert = true;
    }).catch((e) => {
      this.loading = false;
      this.error = true;
this.errorMessage = 'Nu aveți permisiuni pentru anularea proiectului.';  });
  }

  block() {
    this.loading = true;
    this.showAlert = false;
    this.showMessage = false;
    this.error = false;
    this.projectService.blockProject(this.route.snapshot.params['id']).then((r) => {
      this.getProject();
      this.loading = false;
      this.showAlert = true;
    }).catch((e) => {
      this.loading = false;
      this.error = true;
      this.errorMessage ='Nu aveți permisiuni pentru blocarea proiectului.';
    });
  }



  enable() {
    this.loading = true;
    this.showAlert = false;
    this.errorMessage = false;
    this.error = false;
    this.projectService.enableReview(this.route.snapshot.params['id']).then((r) => {
      this.loading = false;
      this.getProject();
      this.showAlert = true;
    }).catch((e) => {
      this.loading = false;
      this.error = true;
      this.errorMessage ='Nu aveți permisiuni pentru marcarea proiectului ca aflat în implementare.';
    });
  }

  public makeComment() {

    if (this.newComment == '') {

    } else {
      // this.frozen = true;
      this.projectService.createComment(this.route.snapshot.params['id'], this.newComment).then((r) => {
        //  console.log(r);

        this.projectService.getProject(this.route.snapshot.params['id'])
          .then((r) => {
            this.project = r;
            console.log(this.project);
            this.firstComment = {
              addr: this.project.hostAddr,
              comm: this.project.description,
              created_at: this.project.creationTime,
              points: 0
            }
          }).catch((e) => {
          });


        this.newComment = '';
        this.comments.push(r['logs'][0].args);

        // this.router.navigate(['']);
      }).catch((e) => {
      });
    }
  }




  getCommentCount() {
    this.projectService.getCommentsCount(this.route.snapshot.params['id']).
      then((r: any) => {
        //console.log(r);
        this.commentCount = r['words'][0];
        //  console.log(this.commentCount);
        // that.isLoading = false;
      }).catch((error) => {
        console.log(error);
      });
    // this.isLoading = false;

    //return this.projectCount;


  }


  getTotalPoints() {
    this.projectService.geTotalPointsCount(this.route.snapshot.params['id']).
      then((r: any) => {
        //console.log(r);
        console.log(r);
        this.points = r['words'][0];
        // console.log(this.project.points);
        //  console.log(this.commentCount);
        // that.isLoading = false;
      }).catch((error) => {
        console.log(error);
      });
    // this.isLoading = false;

    //return this.projectCount;


  }


  getCommentByIndex(id) {
    this.projectService.getComment(this.route.snapshot.params['id'], id).
      then((r: any) => {
        console.log(r);
        this.comments.push(r);
        // this.getClosedProjects();
      }).catch((error) => {
        console.log(error);
      });
  }

  getComments() {

    this.projectService.getCommentsCount(this.route.snapshot.params['id']).
      then((r: any) => {
        let commentCount = r['words'][0];

        for (let i = 1; i <= commentCount; i++) {
          this.getCommentByIndex(i);
        }

        // that.isLoading = false;
      }).catch((error) => {
        console.log(error);
      });
    // this.isLoading = false;

  }

  withdraw() {
    this.loading = true;
    this.projectService.cashoutProjectService(this.route.snapshot.params['id']).then((r) => {
      this.projectService.getProject(this.route.snapshot.params['id'])
        .then((r) => {
          this.project = r;
          console.log(this.project);
          this.firstComment = {
            addr: this.project.hostAddr,
            comm: this.project.description,
            created_at: this.project.creationTime,
            points: 0
          }

        }).catch((e) => {
        });
      this.loading = false;
      console.log(r);
    }).catch((e) => {
      this.loading = false;
      this.error = true;
      this.errorMessage ='Nu pot fi retrase fonduri.';    });
  }



}
