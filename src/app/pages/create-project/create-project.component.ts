import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Project } from 'src/app/models/Project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent implements OnInit {
  account: string;
  address: string;
  amount: string;
  balance: string;
  success: boolean;
  createDone: boolean;
  project = {} as Project;
  issues: [];

  constructor(private projectService: ProjectService, public bsModalRef: BsModalRef, public router: Router) {


  }

  ngOnInit(): void {
    this.getAccountAndBalance();
  }


  getAccountAndBalance = () => {
    // const that = this;
    this.projectService.getUserBalance().
      then(function (retAccount: any) {
        console.log(retAccount);
        this.account = retAccount.account;
        this.balance = retAccount.balance;
        console.log('transfer.components :: getAccountAndBalance :: that.user');
        console.log(this.account);
        // that.isLoading = false;
      }).catch(function (error) {
        console.log(error);
      });
    // this.isLoading = false;
  }



  createProject(e) {
    console.log(this.project);
    this.projectService.createProjectService(this.project).then((r) => {
      //   console.log (r['logs'][0].args['project_id'].words[0]);
      this.router.navigate(['/projects', r['logs'][0].args['projectId'].words[0]]);
      this.bsModalRef.hide();
    }).catch((e) => {
    });


  }




}
