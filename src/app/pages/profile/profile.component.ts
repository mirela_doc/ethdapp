import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  Breadcrumb = [{ name: 'Acasă', to: '/dashboard', active: 'false' }, { name: 'Detalii utilizator', to: '/projects', active: 'true' }]
  constructor(public projectService: ProjectService) { }
  public address: any;
  public originAccount;
  loading: boolean;
  owner: any;
  showAlert: boolean;
  error: boolean;


  ngOnInit(): void {

    this.isOwner();
    // const that = this;
    this.projectService.getUserBalance().
      then((retAccount: any) => {
        //  console.log(retAccount);
        this.originAccount = retAccount.account;

        // that.isLoading = false;
      }).catch(function (error) {
        console.log(error);
      })
    // this.isLoading = false;



  }

  addAddress(e) {
    this.loading = true;
    this.showAlert = false;
    this.error = false;
    this.projectService.addAddress(this.address, this.originAccount).then((r) => {
      this.loading = false;
      this.address = "";
      this.showAlert = true;
    }).catch((e) => {
      this.loading = false;
      this.error = true;
    });
  }


  removeAddress(e) {
    this.loading = true;
    this.error = false;
    this.showAlert = false;
    this.projectService.removeAddress(this.address, this.originAccount).then((r) => {
      this.loading = false;
      this.address = "";
      this.showAlert = true;
    }).catch((e) => {
      this.loading = false;
      this.error = true;
    });
  }

  isOwner() {
    this.projectService.getOwner().then((r) => {
      this.owner = r;
      console.log(this.owner);
    }).catch((e) => {
    });
  }

}
