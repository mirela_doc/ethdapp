import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { GridOptions } from 'ag-grid-community';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { StatusBadgeComponent } from 'src/app/components/status-badge/status-badge.component';
import { ValueFormatterComponent } from 'src/app/components/value-formatter/value-formatter.component';
import { ViewButtonComponent } from 'src/app/components/view-button/view-button.component';
import { ProjectService } from 'src/app/services/project.service';
import { CreateProjectComponent } from '../create-project/create-project.component';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {
  Breadcrumb = [{ name: 'Acasă', to: '/dashboard', active: 'false' }, { name: 'Proiecte', to: '/projects', active: 'true' }];
  rowData: any;
  projects = [];
  projectCount: any;
  bsModalRef: BsModalRef;
  faSearch = faSearch;
  public gridOptions = { pagination: true } as GridOptions;
  search: any;
  nameFilter: any;
  status: any;


  constructor(public projectService: ProjectService, private modalService: BsModalService, private router: Router, private route: ActivatedRoute) {
    switch (router.url) {
      case '/projects/open':
        this.status = 'Propunere';
        break;
      case '/projects':
        this.status = '';
        break;
      case '/projects/implemented':
        this.status = 'Finalizat';
        break;
      default:
        this.status = '';
    }


  }

  ngOnInit(): void {
    this.getProjects();
  }

  columnDefs = [{ field: 'projectId', headerName: 'ID', sortable: true, unSortIcon: true, filter: 'agNumberColumnFilter', width: 100 },
  {
    field: 'name', headerName: 'Subiect', sortable: true, filter: 'agTextColumnFilter', unSortIcon: true,
  },
  {
    field: 'creationTime', headerName: 'Data creare', sortable: true, filter: 'agDateColumnFilter', unSortIcon: true, 
    valueGetter: function (params) {
      const milliseconds = params.data.creationTime * 1000;
      const dateObject = new Date(milliseconds)
      let value = dateObject.toLocaleString()
      return value;
    },
  },
  { field: 'hostAddr', headerName: 'Initiator', sortable: true, filter: 'agTextColumnFilter', unSortIcon: true },
  { field: 'funds', headerName: 'Fonduri colectate', sortable: true, filter: 'agNumberColumnFilter', unSortIcon: true },
  {
    unSortIcon: true, headerName: 'Status', field: 'state', sortable: true, cellRendererFramework: StatusBadgeComponent, filter: 'agTextColumnFilter',
    valueGetter: function (params) {
      let mapping = {
        1: 'Implementare',
        0: 'Propunere',
        2: 'Blocat',
        3: 'Finalizat',
        4: 'Anulat'
      };

      return mapping[params.data.state];
    },

  }
    ,
  { headerName: '', cellRendererFramework: ViewButtonComponent, sortable: false }

  ];

  onFilterTextBoxChanged() {
    if (this.gridOptions.api) {
      this.gridOptions.api.setQuickFilter(this.search);
    }
  }


  getProjects() {
    this.getProjectCount();


    this.projectService.getProjectsCount().
      then((r: any) => {
        let projectCount = r['words'][0];

        for (let i = 1; i <= projectCount; i++) {
          this.getProjectByIndex(i);
        }

        // that.isLoading = false;
      }).catch((error) => {
        console.log(error);
      });
    // this.isLoading = false;

  }


  getProjectByIndex(id) {
    this.projectService.getProject(id).
      then((r: any) => {
        //  console.log(r);
        this.projects.push(r);
        //  this.getClosedProjects();
        if (this.projectCount == this.projects.length) {
          this.rowData = this.projects;
        }

      }).catch((error) => {
        console.log(error);
      });
  }


  getProjectCount() {
    this.projectService.getProjectsCount().
      then((r: any) => {
        this.projectCount = r['words'][0];

        // that.isLoading = false;
      }).catch((error) => {
        console.log(error);
      });
    // this.isLoading = false;



  }

  openModalWithComponent() {
    this.bsModalRef = this.modalService.show(CreateProjectComponent, {

    });
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
    var statusFilter = {
      state: {
        type: 'Equals',
        filter: this.status
      }

    };

    this.gridOptions.api.setFilterModel(statusFilter);
    this.gridOptions.api.onFilterChanged();
  }

  onGridReady(params) {
    this.gridOptions.api = params.api;
    // const statusFilter = params.api.getFilterInstance('state');
    // console.log(this.gridOptions.api.getFilterInstance('state'));
    var statusFilter = {
      state: {
        filter: this.status
      }

    };

    this.gridOptions.api.setFilterModel(statusFilter);
    this.gridOptions.api.onFilterChanged();

  }



}
