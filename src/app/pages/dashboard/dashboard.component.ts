import { Component, OnChanges, OnInit } from '@angular/core';
import { DashletPieComponent } from 'src/app/components/dashlet-pie/dashlet-pie.component';
import { Label } from 'ng2-charts';
import { Dashlet } from 'src/app/models/Dashlet';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  Breadcrumb = [{ name: 'Dashboard', to: '/dashboard', active: 'true' }];
  public projectCount: number;
  projects = [];
  projectsNotif = [];
  noHigh: number;
  noLow: number;
  noMedium: number;
  noOpen: number;
  noClosed: number;
  noBlocked: number;
  noAwaiting: number;
  noAnnulled: number;

  dashletEntityPie2 = {
    name: 'Proiecte în funcție de dificultate',
    data: [],
    labels: ['Scazuta', 'Medie', 'Ridicata']
  };
  dashletEntityPie = {
    name: 'Proiecte în funcție de status',
    data: [],
    labels: ['Propunere', 'Implementare', 'Blocat', 'Finalizat', 'Anulat']
  };

  dashletNumber3: { name: string; value: number; key: string; };
  dashletNumber2: { name: string; value: number; key: string; };
  dashletNumber1: { name: string; value: number; key: string; };

  constructor(private projectService: ProjectService) {
    //   this.getProjectCount();
    // this.getProjects();

    this.dashletEntityPie = {
      name: 'Proiecte în funcție de status',
      data: [],
      labels: ['Propunere', 'Implementare', 'Blocat', 'Finalizat', 'Anulat']
    };


    this.dashletEntityPie2 = {
      name: 'Proiecte în funcție de dificultate',
      data: [],
      labels: ['Scazuta', 'Medie', 'Ridicata']
    };

    this.dashletNumber3 = {
      name: 'Total Proiecte',
      value: this.projectCount,
      key: 'Proiecte'
    };

    this.dashletNumber2 = {
      name: 'Proiecte aflate în implementare',
      value: this.noAwaiting,
      key: 'Proiecte'
    };


    this.dashletNumber1 = {
      name: 'Proiecte în stadiul de propunere',
      value: 0,
      key: 'Proiecte'
    };

  }






  getProjectCount() {
    this.projectService.getProjectsCount().
      then((r: any) => {
        //   console.log(r);
        this.projectCount = r['words'][0];
        //   console.log(this.projectCount);
        this.dashletNumber3.value = this.projectCount;
      }).catch((error) => {
        console.log(error);
      });

  }




  getProjectByIndex(id) {
    this.projectService.getProject(id).
      then((r: any) => {
        //  console.log(r);
        this.projects.push(r);
        this.getClosedProjects();

      }).catch((error) => {
        console.log(error);
      });
  }

  getProjects() {
    this.getProjectCount();


    this.projectService.getProjectsCount().
      then((r: any) => {
        let projectCount = r['words'][0];

        for (let i = 1; i <= projectCount; i++) {
          this.getProjectByIndex(i);

        }
        // that.isLoading = false;
      }).catch((error) => {
        console.log(error);
      });
    // this.isLoading = false;

  }


  ngOnInit(): void {
    this.getNoHigh();
    this.getNoMedium();
    this.getNoLow();
    this.getOpen();
    this.getBlocked();
    this.getAwaiting();
    this.getClosed();
    this.getAnnulled();
    this.getProjectCount();
    this.getProjects();


  }

  getClosedProjects() {
    if (this.projects.length == this.projectCount) {
      this.projectsNotif = this.projects.slice(-4);
    }
  }




  convertDate(date) {
    const milliseconds = date * 1000;
    const dateObject = new Date(milliseconds);
    const value = dateObject.toLocaleString();
    return value;
  }



  getNoLow() {
    this.projectService.getNoLow().
      then((r: any) => {
        console.log(r, 'low');
        this.noLow = r['words'][0];
        this.dashletEntityPie2.data[0] = this.noLow;

        // console.log(this.dashletEntityPie2);
      }).catch((error) => {
        console.log(error);
      });

  }


  getNoMedium() {
    this.projectService.getNoMedium().
      then((r: any) => {
        console.log(r, 'MEDIUM');
        this.noMedium = r['words'][0];
        this.dashletEntityPie2.data[1] = this.noMedium;

      }).catch((error) => {
        console.log(error);
      });

  }

  getNoHigh() {
    this.projectService.getNoHigh().
      then((r: any) => {
        console.log(r, 'high');
        this.noHigh = r['words'][0];
        this.dashletEntityPie2.data[2] = this.noHigh;

      }).catch((error) => {
        console.log(error);
      });
  }



  getOpen() {
    this.projectService.getOpen().
      then((r: any) => {
        //  console.log(r);
        this.noOpen = r['words'][0];
        //    console.log(this.noOpen);
        this.dashletNumber1.value = this.noOpen;
        this.dashletEntityPie.data[0] = this.noOpen;
        console.log(this.noOpen, 'open');

        //console.log(this.noOpen);
      }).catch((error) => {
        console.log(error);
      });




    //console.log(this.noOpen);

    // this.isLoading = false;
  }



  getClosed() {
    this.projectService.getImplemented().
      then((r: any) => {
        this.noClosed = r['words'][0];
        // this.dashletNumber2.value = this.noClosed;
        this.dashletEntityPie.data[3] = this.noClosed;
console.log("closed", this.noClosed);
        //  console.log(this.noOpen);
      }).catch((error) => {
        console.log(error);
      });
    // this.isLoading = false;
  }


  getBlocked() {
    this.projectService.getBlocked().
      then((r: any) => {
        this.noBlocked = r['words'][0];
        console.log(this.noBlocked);
        this.dashletEntityPie.data[2] = this.noBlocked;
        console.log(this.noBlocked, 'blocked');

        // console.log(this.noOpen);
      }).catch((error) => {
        console.log(error);
      });
    // this.isLoading = false;
  }

  getAwaiting() {
    this.projectService.getAwaiting().
      then((r: any) => {
        this.noAwaiting = r['words'][0];
        this.dashletNumber2.value = this.noAwaiting;
        this.dashletEntityPie.data[1] = this.noAwaiting;
        console.log("Awaiting", this.noAwaiting);

        //  console.log(this.noOpen);
      }).catch((error) => {
        console.log(error);
      });
    // this.isLoading = false;
  }


  getAnnulled() {
    this.projectService.getAnnulled().
      then((r: any) => {
        //  console.log(r);
        this.noAnnulled = r['words'][0];

        this.dashletEntityPie.data[4] = this.noAnnulled;
        console.log("annuled", this.noAnnulled);
      }).catch((error) => {
        console.log(error);
      });

  }



}
