module.exports = {
  networks: {
    genache: {
      host: "localhost",
      port: 7545,
      gas: 5000000,
      network_id: "*",
      block: 4
    }
  },
  compilers: {
    solc: {
      version: "0.7.4"  
}}
}
