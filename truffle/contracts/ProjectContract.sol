pragma solidity 0.7.4;
pragma experimental ABIEncoderV2;

//import "openzeppelin-solidity/contracts/math/SafeMath.sol";

library SafeMath {
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) {
            return 0;
        }
        uint256 c = a * b;
        assert(c / a == b);
        return c;
    }

    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // assert(b > 0); // Solidity automatically throws when dividing by 0
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold
        return c;
    }

    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        assert(b <= a);
        return a - b;
    }

    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        assert(c >= a);
        return c;
    }
}

contract ProjectContract {
    // Events
    event ProjectCreation(
        address projectHost,
        uint256 projectId,
        string description,
        string name,
        uint256 creationTime
    );

    event SponsorshipSubmited(uint256 projectId, uint256 value);

    event ProjectClosed(uint256 projectId);

    event ProjectBlocked(uint256 projectId);

    event ProjectAnnulled(uint256 projectId);

    event CommentCreated(uint256 projectId, address addr, string comm);

    event ProjectReviewEnabled(uint256 projectId);

    event RatingSubmited(
        uint256 projectId,
        address reviewerAddr,
        uint256 commentId,
        uint256 points
    );

    event Whitelisted(address indexed account, bool isWhitelisted);

    event CashOut(uint256 projectId, address addr, uint256 reward);

    using SafeMath for uint256;
    // Structs
    struct Project {
        address hostAddr;
        uint256 projectId;
        ProjectState state;
        string description;
        string name;
        uint256 funds;
        uint256 creationTime;
        uint256 commentId;
        Difficulty difficulty;
        uint256 withdrawnFunds;
        uint256 duration;
        uint256 cost;
    }

    struct Comment {
        address addr;
        uint256 points;
        string comm;
        // uint256 comm_id;
        // uint256 created_at;
    }

    // Enums
    enum ProjectState {Open, ReviewEnabled, Blocked, Closed, Annulled}
    enum Difficulty {Low, Medium, High}

    // Public variables
    address public owner;
    mapping(uint256 => Project) public projects; // Stores projects data
    mapping(uint256 => mapping(uint256 => Comment)) public comments;
    uint256 public fee = 0.01 ether;
    uint256 public projectCount;
    uint256 public noLow;
    uint256 public noMedium;
    uint256 public noHigh;
    uint256 public noBlocked;
    uint256 public noOpen;
    uint256 public noImplemented;
    uint256 public noAwaiting;
    uint256 public noAnnulled;
    mapping(uint256 => mapping(address => mapping(uint256 => uint256)))
        public ratings;
    mapping(uint256 => uint256) public totalProjectPoints;
    mapping(uint256 => mapping(address => uint256))
        public totalParticipantPoints;

    mapping(uint256 => mapping(address => bool))
        public participantHasCashedOut;
    mapping(address => bool) private whitelistedMap;

    // Modifiers
    modifier isClosed(uint256 index) {
        require(
            projects[index].state == ProjectState.Closed,
            "Project is not closed"
        );
        _;
    }

    modifier isNotClosed(uint256 index) {
        require(
            projects[index].state != ProjectState.Closed,
            "Project is closed"
        );
        _;
    }

    modifier isOpen(uint256 index) {
        require(
            projects[index].state == ProjectState.Open,
            "Project is not open"
        );
        _;
    }

    modifier isOwner(uint256 index) {
        require(projects[index].hostAddr == msg.sender, "Not Owner");
        _;
    }

    modifier pay() {
        require(msg.value == fee, "Pay fee");
        _;
    }

    modifier pointsAreValid(uint256 points) {
        require(points <= 5, "Points > 5");
        _;
    }

    modifier commentsExists(uint256 projectId, uint256 comment_id) {
        require(
            comments[projectId][comment_id].addr != address(0),
            "Comments do not exist"
        );
        _;
    }

    modifier isReviewEnabled(uint256 index) {
        require(
            projects[index].state == ProjectState.ReviewEnabled,
            "Project is not review enabled"
        );
        _;
    }

    modifier hasNotCashedOut(uint256 projectId, address addr) {
        require(
            !participantHasCashedOut[projectId][addr],
            "Participant has already cashed out"
        );
        _;
    }

    modifier onlyOwner {
        require(isOnlyOwner(), "Only owner can do that!");
        _;
    }

    modifier isWhiteListed(address addr) {
        require(whitelistedMap[addr] == true, "Only white listed can do that!");
        _;
    }

    // Public methods

    constructor() {
        owner = msg.sender;
    }

    function whitelisted(address _address) external view returns (bool) {
        return whitelistedMap[_address];
    }

    function addAddress(address _address) external onlyOwner {
        require(whitelistedMap[_address] != true);
        whitelistedMap[_address] = true;
        emit Whitelisted(_address, true);
    }

    function removeAddress(address _address) external onlyOwner {
        require(whitelistedMap[_address] != false);
        whitelistedMap[_address] = false;
        emit Whitelisted(_address, false);
    }

    function isOnlyOwner() private view returns (bool) {
        return msg.sender == owner;
    }

    function createProject(
        string memory _description,
        string memory _name,
        Difficulty _diff,
        uint256 _duration,
        uint256 _cost
    ) external {
        projectCount += 1;
        uint256 dateNow = block.timestamp;

        projects[projectCount] = Project(
            msg.sender,
            projectCount,
            ProjectState.Open,
            _description,
            _name,
            0,
            dateNow,
            0,
            _diff,
            0,
            _duration,
            _cost
        );

        if (_diff == Difficulty.Low) {
            noLow += 1;
        } else if (_diff == Difficulty.Medium) {
            noMedium += 1;
        } else if (_diff == Difficulty.High) {
            noHigh += 1;
        }
        noOpen += 1;
        emit ProjectCreation(
            msg.sender,
            projectCount,
            _description,
            _name,
            dateNow
        );
    }

    function createComment(uint256 projectId, string memory _comm)
        external
        payable
        pay
    //  isOpen(projectId)
    {
        //  uint256 date_now = now;
        //   Comment memory comment = Comment(msg.sender, 0, _comm);
        projects[projectId].commentId += 1;
        Comment memory comment = Comment(msg.sender, 0, _comm);
        comments[projectId][projects[projectId].commentId] = comment;
        projects[projectId].funds = projects[projectId].funds.add(fee);

        emit CommentCreated(projectId, msg.sender, _comm);
    }

    function getProjectByIndex(uint256 index)
        external
        view
        returns (Project memory project)
    {
        //  require(index < project_count);
        return (projects[index]);
    }

    function getTotalProjectPoints(uint256 index)
        external
        view
        returns (uint256)
    {
        return totalProjectPoints[index];
    }

    function getCommentsProjectCount(uint256 index)
        external
        view
        returns (uint256)
    {
        return projects[index].commentId;
    }

    function getCommentsByProject(uint256 project_index, uint256 commentIndex)
        external
        view
        returns (
            address addr,
            uint256 points,
            string memory comm
        )
    //  uint256 created_at
    {
        return (
            comments[project_index][commentIndex].addr,
            comments[project_index][commentIndex].points,
            comments[project_index][commentIndex].comm
            //  comments[project_index][comment_index].created_at
        );
    }

    function getProjectsCount() external view returns (uint256) {
        return projectCount;
    }

    function sponsorProject(uint256 index) external payable isNotClosed(index) {
        projects[index].funds = projects[index].funds.add(msg.value);
        emit SponsorshipSubmited(index, msg.value);
    }

    function closeProject(uint256 index) external isWhiteListed(msg.sender) {
        projects[index].state = ProjectState.Closed;
        noImplemented += 1;
        noAwaiting -= 1;
        emit ProjectClosed(index);
    }

    function blockProject(uint256 index) external isWhiteListed(msg.sender) {
        projects[index].state = ProjectState.Blocked;
        noBlocked += 1;
        noAwaiting -= 1;
        emit ProjectBlocked(index);
    }

    function annulProject(uint256 index) external isWhiteListed(msg.sender) {
        if (projects[index].state == ProjectState.Open) {
            noOpen -= 1;
        } else {
            noBlocked -= 1;
        }
        projects[index].state = ProjectState.Annulled;
        noAnnulled += 1;
        emit ProjectAnnulled(index);
    }

    function enableProjectReview(uint256 projectId)
        external
        isWhiteListed(msg.sender)
    {
        if (projects[projectId].state == ProjectState.Open) {
            noOpen -= 1;
        } else {
            noBlocked -= 1;
        }
        projects[projectId].state = ProjectState.ReviewEnabled;
        noAwaiting += 1;
        emit ProjectReviewEnabled(projectId);
    }

    function rate(
        uint256 projectId,
        address addr,
        uint256 points,
        uint256 commentId
    )
        external
        //   commentsExists(projectId, comment_id)
        pointsAreValid(points)
    // isReviewEnabled(projectId)
    {
        uint256 ratingStored = ratings[projectId][msg.sender][commentId];
        comments[projectId][commentId].points = comments[projectId][
            commentId
        ]
            .points
            .add(points)
            .sub(ratingStored);
        totalProjectPoints[projectId] = totalProjectPoints[projectId]
            .add(points)
            .sub(ratingStored);

        totalParticipantPoints[projectId][addr] = totalParticipantPoints[
            projectId
        ][addr]
            .add(points)
            .sub(ratingStored);
        ratings[projectId][addr][commentId] = points;
        emit RatingSubmited(projectId, msg.sender, commentId, points);
    }

    function cashOut(uint256 projectId)
        external
        hasNotCashedOut(projectId, msg.sender)
      //  isClosed(projectId)
    {
        uint256 totalPoints = totalProjectPoints[projectId];

        uint256 points = totalParticipantPoints[projectId][msg.sender];

        // Calculate reward
        uint256 funds = projects[projectId].funds;
        uint256 withdrawnFunds = projects[projectId].withdrawnFunds;
        uint256 myReward = funds.mul(points).div(totalPoints);
        emit CashOut(projectId, msg.sender, myReward);
        participantHasCashedOut[projectId][msg.sender] = true;
        projects[projectId].withdrawnFunds = withdrawnFunds.add(myReward);
        msg.sender.transfer(myReward);
    }

    function myPoints(
        uint256 projectId,
        address addr,
        uint256 comment_id
    ) external view returns (uint256) {
        return ratings[projectId][addr][comment_id];
    }

    function getTotalNoLow() external view returns (uint256) {
        return noLow;
    }

    function getTotalNoHigh() external view returns (uint256) {
        return noHigh;
    }

    function getTotalNoMedium() external view returns (uint256) {
        return noMedium;
    }

    function getOpen() external view returns (uint256) {
        return noOpen;
    }

    function getAwaiting() external view returns (uint256) {
        return noAwaiting;
    }

    function getBlocked() external view returns (uint256) {
        return noBlocked;
    }

    function getImplemented() external view returns (uint256) {
        return noImplemented;
    }

    function getAnnulled() external view returns (uint256) {
        return noAnnulled;
    }

    function getOwner() external view returns (address) {
        return owner;
    }

    // fallback() external payable {
    //     revert();
    // }

    receive() external payable {
        revert();
    }
}
